package handlers

import (
	"bufio"
	"fmt"
	"os"
	"strings"

	"bitbucket.org/billing/go-gen-l3/models"
)

// GenL3 :
type GenL3 struct {
	ListProcess   *models.ProcessGenerateL3
	DataMatch     *[]models.StructDataL3Match
	DataDupTAPIN  *[]models.StructDataL3
	DataDupOCS    *[]models.StructDataL3
	DataOnlyTAPIN *[]models.StructDataL3
	DataOnlyOCS   *[]models.StructDataL3
}

// DoJobs :
func (p *GenL3) DoJobs() error {
	// Write TAPIN Duplicate L3
	fmt.Println(" [ Writing TAPIN Dup File ] : " + p.ListProcess.PathTAPINDup + p.ListProcess.FileTAPINDup)
	err := p.WriteDataL3(p.ListProcess.PathTAPINDup, p.ListProcess.FileTAPINDup, p.DataDupTAPIN)
	if err != nil {
		fmt.Println("   > Failed, ", err.Error())
		return err
	}
	fmt.Println("   > Done")

	// Write OCS Duplicate L3
	fmt.Println(" [ Writing OCS Dup File ] : " + p.ListProcess.PathOCSDup + p.ListProcess.FileOCSDup)
	err = p.WriteDataL3(p.ListProcess.PathOCSDup, p.ListProcess.FileOCSDup, p.DataDupOCS)
	if err != nil {
		fmt.Println("   > Failed, ", err.Error())
		return err
	}
	fmt.Println("   > Done")

	// Write TAPIN Only L3
	fmt.Println(" [ Writing TAPIN Only File ] : " + p.ListProcess.PathTAPINOnly + p.ListProcess.FileTAPINOnly)
	err = p.WriteDataL3(p.ListProcess.PathTAPINOnly, p.ListProcess.FileTAPINOnly, p.DataOnlyTAPIN)
	if err != nil {
		fmt.Println("   > Failed, ", err.Error())
		return err
	}
	fmt.Println("   > Done")

	// Write OCS Only L3
	fmt.Println(" [ Writing OCS Only File ] : " + p.ListProcess.PathOCSOnly + p.ListProcess.FileOCSOnly)
	err = p.WriteDataL3(p.ListProcess.PathOCSOnly, p.ListProcess.FileOCSOnly, p.DataOnlyOCS)
	if err != nil {
		fmt.Println("   > Failed, ", err.Error())
		return err
	}
	fmt.Println("   > Done")

	// Write Match L3
	fmt.Println(" [ Writing Match File ] : " + p.ListProcess.PathMatch + p.ListProcess.FileMatch)
	err = p.WriteDataL3(p.ListProcess.PathMatch, p.ListProcess.FileMatch, p.DataMatch)
	if err != nil {
		fmt.Println("   > Failed, ", err.Error())
		return err
	}
	fmt.Println("   > Done")

	return nil
}

// WriteDataL3 :
func (p *GenL3) WriteDataL3(pathFile, fileName string, inputData interface{}) error {
	// Making Directory & Sub Directories
	if _, err := os.Stat(pathFile); os.IsNotExist(err) {
		fmt.Printf("   > Making Directory : %s => ", pathFile)
		err = os.MkdirAll(pathFile, 0777)
		if err != nil {
			fmt.Println("Failed, ", err.Error())
			return err
		}
		fmt.Println("Success")
	}

	// Opening & Creating File
	inputFile := pathFile + fileName
	fileOpen, err := os.Create(inputFile)
	if err != nil {
		return err
	}
	defer fileOpen.Close()

	// Bufio Writer File
	buffWriter := bufio.NewWriter(fileOpen)

	switch l3Data := inputData.(type) {
	case *[]models.StructDataL3:
		for _, v := range *l3Data {
			imsi := strings.Replace(v.Imsi, "Event: ", "", 1)
			fmt.Fprintln(buffWriter, v.PartnerID, v.StartTime, v.Anum, v.Bnum, imsi, v.Duration, v.Charge)
		}
	case *[]models.StructDataL3Match:
		for _, v := range *l3Data {
			fmt.Fprintln(buffWriter,
				v.PartnerID,
				v.StartTimeTapin,
				v.AnumTapin,
				v.BnumTapin,
				v.ImsiTapin,
				v.DurationTapin,
				v.ChargeTapin,
				v.StartTimeOCS,
				v.AnumOCS,
				v.BnumOCS,
				v.ImsiOCS,
				v.DurationOCS,
				v.ChargeOCS,
				v.TimeDiff,
				v.DurationDiff,
				v.ChargeDiff,
			)
		}
	default: // If type unrecognized
		break
	}

	// File Bufio Flush
	err = buffWriter.Flush()
	if err != nil {
		return err
	}

	return nil
}
