package models

// ProcessGenerateL3 :
type ProcessGenerateL3 struct {
	ProcessID         string `json:"process_id"`
	BatchID           string `json:"batch_id"`
	Periode           string `json:"periode"`
	PostPre           string `json:"post_pre"`
	MoMt              string `json:"mo_mt"`
	FileTypeMatch     int    `json:"filetype_match"`
	PathMatch         string `json:"path_match"`
	FileMatch         string `json:"file_match"`
	FileTypeOCSOnly   int    `json:"filetype_ocs_only"`
	PathOCSOnly       string `json:"path_ocs_only"`
	FileOCSOnly       string `json:"file_ocs_only"`
	FileTypeTAPINOnly int    `json:"filetype_tapin_only"`
	PathTAPINOnly     string `json:"path_tapin_only"`
	FileTAPINOnly     string `json:"file_tapin_only"`
	FileTypeOCSDup    int    `json:"filetype_ocs_dup"`
	PathOCSDup        string `json:"path_ocs_dup"`
	FileOCSDup        string `json:"file_ocs_dup"`
	FileTypeTAPINDup  int    `json:"filetype_tapin_dup"`
	PathTAPINDup      string `json:"path_tapin_dup"`
	FileTAPINDup      string `json:"file_tapin_dup"`
}

// StructDataL3 :
type StructDataL3 struct {
	PartnerID string `json:"partner_id"`
	StartTime string `json:"start_time"`
	Anum      string `json:"a_num"`
	Bnum      string `json:"b_num"`
	Imsi      string `json:"imsi"`
	Duration  int    `json:"duration"`
	Charge    string `json:"charge"`
}

// StructDataL3Match :
type StructDataL3Match struct {
	PartnerID      string  `json:"partner_id"`
	StartTimeTapin string  `json:"start_time_tapin"`
	AnumTapin      string  `json:"a_num_tapin"`
	BnumTapin      string  `json:"b_num_tapin"`
	ImsiTapin      string  `json:"imsi_tapin"`
	DurationTapin  int     `json:"duration_tapin"`
	ChargeTapin    string  `json:"charge_tapin"`
	StartTimeOCS   string  `json:"start_time_ocs"`
	AnumOCS        string  `json:"a_num_ocs"`
	BnumOCS        string  `json:"b_num_ocs"`
	ImsiOCS        string  `json:"imsi_ocs"`
	DurationOCS    int     `json:"duration_ocs"`
	ChargeOCS      string  `json:"charge_ocs"`
	TimeDiff       float64 `json:"time_diff"`
	DurationDiff   int     `json:"duration_diff"`
	ChargeDiff     float64 `json:"charge_diff"`
}

// CloseStruct :
type CloseStruct struct {
	ProcessConf     *ProcessGenerateL3 `json:"process_conf"`
	ProcessStatusID int                `json:"process_status_id"`
	ErrorMessage    string             `json:"error_message"`
}
