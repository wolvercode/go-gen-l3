package models

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"net/http"

	"bitbucket.org/billing/go-gen-l3/app"
)

// GetProcess :
func GetProcess() ([]ProcessGenerateL3, error) {
	var listProcess []ProcessGenerateL3

	response, err := http.Get(app.Appl.DBAPIURL + "process/generatel3/getProcess")
	if err != nil {
		fmt.Printf("The HTTP request failed with error %s\n", err)
		return nil, err
	}
	defer response.Body.Close()

	defer io.Copy(ioutil.Discard, response.Body)

	json.NewDecoder(response.Body).Decode(&listProcess)

	return listProcess, nil
}

// UpdateStatus :
func UpdateStatus(processID, processStatusID string) error {
	client := &http.Client{}
	response, err := http.NewRequest(http.MethodPut, app.Appl.DBAPIURL+"process/updateStatus/"+processID+"/"+processStatusID, bytes.NewBuffer(nil))
	if err != nil {
		fmt.Printf("The HTTP request failed with error %s\n", err)
		return err
	}
	_, err = client.Do(response)
	if err != nil {
		// handle error
		log.Fatal(err)
	}
	defer io.Copy(ioutil.Discard, response.Body)
	defer response.Body.Close()

	return nil
}

// GetDataL3 :
func GetDataL3(periode, postPre, moMt, dataType string) ([]StructDataL3, error) {
	var listProcess []StructDataL3

	response, err := http.Get(app.Appl.DBAPIURL + "process/generatel3/getData/" + periode + "/" + postPre + "/" + moMt + "/" + dataType)
	if err != nil {
		fmt.Printf("The HTTP request failed with error %s\n", err)
		return nil, err
	}
	defer response.Body.Close()

	defer io.Copy(ioutil.Discard, response.Body)

	json.NewDecoder(response.Body).Decode(&listProcess)

	return listProcess, nil
}

// GetDataL3Match :
func GetDataL3Match(periode, postPre, moMt string) ([]StructDataL3Match, error) {
	var listProcess []StructDataL3Match

	response, err := http.Get(app.Appl.DBAPIURL + "process/generatel3/getDataMatch/" + periode + "/" + postPre + "/" + moMt)
	if err != nil {
		fmt.Printf("The HTTP request failed with error %s\n", err)
		return nil, err
	}
	defer response.Body.Close()

	defer io.Copy(ioutil.Discard, response.Body)

	json.NewDecoder(response.Body).Decode(&listProcess)

	return listProcess, nil
}

// CloseProcess :
func CloseProcess(closeStruct *CloseStruct) error {
	jsonValue, _ := json.Marshal(closeStruct)
	response, err := http.Post(app.Appl.DBAPIURL+"process/generatel3/closeProcess/", "application/json", bytes.NewBuffer(jsonValue))
	if err != nil {
		fmt.Printf("The HTTP request failed with error %s\n", err)
		return err
	}
	io.Copy(ioutil.Discard, response.Body)
	defer response.Body.Close()

	return nil
}
