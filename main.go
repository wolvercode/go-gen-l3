package main

import (
	"fmt"

	"bitbucket.org/billing/go-gen-l3/app"
	"bitbucket.org/billing/go-gen-l3/handlers"
	"bitbucket.org/billing/go-gen-l3/models"
)

func main() {
	for {
		app.Initialize()

		flowControl()

		app.Sleeping()
	}
}

func flowControl() error {
	fmt.Printf("[ Getting Jobs From Database ] : ")
	listProcess, err := models.GetProcess()
	if err != nil {
		fmt.Println(err.Error())
		return err
	}

	if listProcess == nil {
		fmt.Println("No job found in database")
		return nil
	}

	fmt.Println(len(listProcess), "Jobs")

	for _, v := range listProcess {
		fmt.Println("[ Processing ] : { Process ID :", v.ProcessID, "}")

		// Update Process Status ID to 4 (On Process)
		models.UpdateStatus(v.ProcessID, "4")

		fmt.Printf(" [ Get Data L3 TAPIN Duplicate ] : ")
		tapinDup, err := models.GetDataL3(v.Periode, v.PostPre, v.MoMt, "TAPIN_DUP")
		if err != nil {
			fmt.Println("Failed, with error :", err.Error())
			continue
		}
		fmt.Println("Success")

		fmt.Printf(" [ Get Data L3 OCS Duplicate ] : ")
		ocsDup, err := models.GetDataL3(v.Periode, v.PostPre, v.MoMt, "OCS_DUP")
		if err != nil {
			fmt.Println("Failed, with error :", err.Error())
			continue
		}
		fmt.Println("Success")

		fmt.Printf(" [ Get Data L3 TAPIN Only ] : ")
		tapinOnly, err := models.GetDataL3(v.Periode, v.PostPre, v.MoMt, "TAPIN_ONLY")
		if err != nil {
			fmt.Println("Failed, with error :", err.Error())
			continue
		}
		fmt.Println("Success")

		fmt.Printf(" [ Get Data L3 OCS Only ] : ")
		ocsOnly, err := models.GetDataL3(v.Periode, v.PostPre, v.MoMt, "OCS_ONLY")
		if err != nil {
			fmt.Println("Failed, with error :", err.Error())
			continue
		}
		fmt.Println("Success")

		fmt.Printf(" [ Get Data L3 Match ] : ")
		dataMatch, err := models.GetDataL3Match(v.Periode, v.PostPre, v.MoMt)
		if err != nil {
			fmt.Println("Failed, with error :", err.Error())
			continue
		}
		fmt.Println("Success")

		handler := handlers.GenL3{
			ListProcess:   &v,
			DataDupTAPIN:  &tapinDup,
			DataDupOCS:    &ocsDup,
			DataOnlyTAPIN: &tapinOnly,
			DataOnlyOCS:   &ocsOnly,
			DataMatch:     &dataMatch,
		}

		closeStruct := models.CloseStruct{ProcessConf: &v, ProcessStatusID: 4}

		// Do jobs of write l3 data
		err = handler.DoJobs()
		if err != nil {
			// Process Status ID 5 is Failed
			closeStruct.ProcessStatusID = 5
			closeStruct.ErrorMessage = err.Error()
		} else {
			// Process Status ID 7 is Success
			closeStruct.ProcessStatusID = 7
		}

		// Closing current process
		models.CloseProcess(&closeStruct)
	}

	return nil
}
